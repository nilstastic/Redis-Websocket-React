var io = require('socket.io').listen(5001);
var redis = require('redis');

var redisSubscribeClient = redis.createClient("redis://127.0.0.1:6379");
var redisDataClient = redis.createClient("redis://127.0.0.1:6379");

io.on("connection", function (socket) {
    console.log("client connected");

    socket.on("subscribe", function (room) {
        console.log("joining " + room);
        socket.join(room);
    });

    socket.on("unsubscribe", function (room) {
        console.log("leaving " + room);
        socket.leave(room);
    });

    socket.on("message", function (message) {
        console.log("message from client");
        console.log(message);
        socket.filters = message;
    });
});

redisSubscribeClient.on("message", function (channel, message) {
    console.log(channel + ":" + message);

    redisDataClient.hgetall(message, function (error, reply) {
        console.log(reply)
        io.sockets.in(channel).emit("message", reply)
    });
})

// Vi lyssnar på meddelanden från följande kanaler
redisSubscribeClient.subscribe("tennis");
redisSubscribeClient.subscribe("golf");
redisSubscribeClient.subscribe("football");