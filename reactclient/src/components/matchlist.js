import React from "react";
import Match from "./match";
import style from "./matchlist.css";

export default props => {
  return (
    <table className={style.table}>
      <tbody>
        {Object.keys(props.matches).map(key => <Match key={key} {...props.matches[key]} />)}
      </tbody>
    </table>
  );
};
