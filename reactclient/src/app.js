import {hot} from "react-hot-loader";
import React, {Component} from "react";
import ReactDOM from "react-dom";
import FilterList from "./components/filterlist";
import MatchList from "./components/matchlist";
import {subscribe, setfilter, unsetfilter} from "./io/client";
import style from "./app.css";

class App extends Component {
  constructor(props) {
    super(props);

    subscribe(msg => this.handleMessage(msg));

    this.state = {
      sportfilters: [
        {id: "tennis", label: "Tennis"},
        {id: "golf", label: "Golf"},
        {id: "football", label: "Football"}
      ],
      companyfilters: [
        {id: "betfair", label: "Betfair"},
        {id: "svenskaspel", label: "Svenska spel"},
        {id: "bet365", label: "Bet365"}
      ],
      matches: {}
    };
  }

  handleFilterChange(filter) {
    if (filter.selected) {
      setfilter(filter.id);
    } else {
      unsetfilter(filter.id);
    }
  }

  handleMessage(msg) {
    const matches = {...this.state.matches};
    matches[msg.id] = msg;
    this.setState({matches: matches});
  }

  render() {
    return (
      <div className={style.main}>
        <header className={style.header}>
          <div className={style.filterlist}>
            <h3>Sports</h3>
            <FilterList filters={this.state.sportfilters} onChange={this.handleFilterChange} />
          </div>
          <div className={style.filterlist}>
            <h3>Companies</h3>
            <FilterList filters={this.state.companyfilters} onChange={this.handleFilterChange} />
          </div>
        </header>
        <section className={style.maincolumn}>
          <h3>Matches</h3>
          <MatchList matches={this.state.matches} />
        </section>
      </div>
    );
  }
}

export default App;
