import React from "react";
import io from "socket.io-client";

const socket = io("http://127.0.0.1:5001");

const subscribe = callback => {
  socket.on("message", callback);
};

const setfilter = filter => {
  socket.emit("subscribe", filter);
};

const unsetfilter = filter => {
  socket.emit("unsubscribe", filter);
};

export {setfilter};
export {unsetfilter};
export {subscribe};
