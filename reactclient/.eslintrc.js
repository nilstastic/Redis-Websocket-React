module.exports = {
  extends: "airbnb",
  rules: {
    quotes: [1, "double"],
    "func-names": ["error", "never"],
    "linebreak-style": ["error", "windows"],
    indent: "off" /*["error", 4],*/,
    "comma-dangle": ["error", "never"],
    "object-curly-spacing": "off" /* ["error", "always"],*/,
    "arrow-body-style": "off",
    camelcase: "off",
    "react/jsx-indent": "off",
    "react/prop-types": "off",
    "react/jsx-filename-extension": [1, {extensions: [".js", ".jsx"]}]
  }
};
