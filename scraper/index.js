const client = require("redis").createClient("redis://127.0.0.1:6379");

client.on("connect", function() {
  console.log("Connected to redis");
});

const scrapingresult = [
  {
    id: 1,
    homeName: "Sirius U21",
    homeOdds: 1.083,
    awayName: "Örebro SK U21",
    awayOdds: 15
  },
  {
    id: 2,
    homeName: "Hibernian",
    homeOdds: 1.8,
    awayName: "Hearts",
    awayOdds: 4.5
  },
  {
    id: 3,
    homeName: "Girona",
    homeOdds: 1.55,
    awayName: "Deportivo La Coruna",
    awayOdds: 5.5
  }
];

setInterval(
  function() {
    const i = Math.floor(Math.random() * scrapingresult.length);
    scrapingresult[i].homeOdds += Math.round(Math.random() * 10) / 100;
    scrapingresult[i].awayOdds += Math.round(Math.random() * 10) / 100;
    client.hmset(scrapingresult[i].id, scrapingresult[i]);
    client.publish("football", scrapingresult[i].id);
  }.bind(this),
  250
);
