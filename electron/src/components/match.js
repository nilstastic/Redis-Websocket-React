import React from "react";
import style from "./match.css";

export default props => {
  return (
    <tr className={style.row}>
      <td>
        {props.homeName} - {props.awayName}
      </td>
      <td>
        {Math.round(props.homeOdds * 100) / 100} /
        {Math.round(props.awayOdds * 100) / 100}
      </td>
    </tr>
  );
};
