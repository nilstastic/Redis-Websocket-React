import React from "react";

export default props => {
  return (
    <div>
      <input
        type="checkbox"
        id={props.id}
        onChange={e => props.onChange({id: e.target.id, selected: e.target.checked})}
      />
      <label htmlFor={props.id}>{props.label}</label>
    </div>
  );
};
