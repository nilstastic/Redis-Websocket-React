import React from "react";
import Filter from "./filter";

export default props => {
  return (
    <ul>
      {props.filters.map(item => (
        <li key={item.id}>
          <Filter label={item.label} id={item.id} onChange={props.onChange} />
        </li>
      ))}
    </ul>
  );
};
