# Möjlig lösning för redis + websockets + react

Det här är ett testskott för en tänkbar lösning för att kunna använda redis och websockets för att visa arbs med så låg latency som möjligt. Nedlagd tid är ca 4 timmar (med feber!) och jag har försökt hålla det så enkelt som möjligt men att ändå få med saker som behövs för att man skall fatta hur saker och ting hänger ihop.

## Applikationen består av fyra delar.

1.  Redis som ligger på 127.0.0.1:6379, körs i mitt fall som en dockercontainer.
2.  En "scraper" vars uppgift är att löpande trycka in slumpad data i redis.
3.  En websockerserver som lyssar på uppdaterad data från redis och skickar vidare datan till de klienter som är intresserade
4.  En klientapplikation i react som kopplar upp sig mot websocketservern och lyssnar på inkommande data. Den kan även välja att filtrera på viss data med fina checkboxar.
5.  En electronapplikation som wrappar en reactapplikation, kan vara intressant att se hur man kan använda samma kodbas för en desktopapp och en webapp.

## För att komma igång.

1.  Installera node https://nodejs.org/en/download/current/
2.  Klona hela projektet (eller ladda ner)
3.  Starta redis `docker run -p 6379:6379 --name test-redis -d redis`
4.  Skriv npm install i scraper, server och klientmappen (samt electronmappen). Samma effekt som restore i dotnet.
5.  Starta "scrapern" `node index.js`
6.  Starta websocketserver `node index.js`
7.  Starta klientappen `npm run dev`
8.  För att starta electronappen måste man först bygga med `npm run dev` och sedan starta applikationen med `npm run start`.

## Vidare funderingar

1.  Just nu kör vi socketservern på en port (kommer inte ihåg vilken..), men iaf. Det man vill göra är att köra den på :80 så att man tar sig igenom så många brandväggar som möjligt. En tänkbar setup där är att t.ex. ha en nginx front som agerar proxy för webbserver och socketserver.
2.  Att inte använda socket.io är tänkbart men det finns ett antal fördelar, bland annat så faller den tillbaka på long polling om den inte lyckas få till en websocket-uppkoppling.
3.  Glöm inte bort autentisering (jwt)
4.  Glöm inte bort tls (wss://)
5.  Det här bör skala bra eftersom man kan skala ut websocketservern hur mycket som helst, men 500 samtidiga bör den ta lätt.
6.  En bra byggpipeline kan man få genom att t.ex. använda gitlab ci.
